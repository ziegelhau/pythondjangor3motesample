from companies import tank_level_company_factory
from enums import PumpStatus, TankStatus
from models import Company1MetaData
from testlib import jsonAlmostEqual


def test_jsonAlmostEqual():
    # Test that custom function I made works
    a = []
    b = []
    assert jsonAlmostEqual(a, b)
    a = {}
    b = {}
    assert jsonAlmostEqual(a, b)
    a = {"1": "a"}
    b = {}
    assert not jsonAlmostEqual(a, b)
    a = {"1": "a"}
    b = {"1": "a"}
    assert jsonAlmostEqual(a, b)
    a = {"1": "a"}
    b = {"1": "b"}
    assert not jsonAlmostEqual(a, b)
    a = {"1": "1.23"}
    b = {"1": "1.23"}
    assert jsonAlmostEqual(a, b)
    a = {"1": "1.234"}
    b = {"1": "1.23"}
    assert not jsonAlmostEqual(a, b)
    a = {"1": 1.000000000000001, "2": "a"}
    b = {"1": 1.000000000000002, "2": "a"}
    assert not jsonAlmostEqual(a, b, rel_tol=1e-20)
    assert jsonAlmostEqual(a, b, rel_tol=1e-8)
    assert jsonAlmostEqual(a, b)
    # Nested dicts
    a = {"1": {"2": 1.000000000000001}}
    b = {"1": {"2": 1.000000000000002}}
    assert not jsonAlmostEqual(a, b, rel_tol=1e-20)
    assert jsonAlmostEqual(a, b, rel_tol=1e-8)
    assert jsonAlmostEqual(a, b)
    a = {"1": {"2": 1.000000000000001, "3": "a"}, "2": "1.23"}
    b = {"1": {"2": 1.000000000000002, "3": "a"}, "2": "1.23"}
    assert not jsonAlmostEqual(a, b, rel_tol=1e-20)
    assert jsonAlmostEqual(a, b, rel_tol=1e-8)
    assert jsonAlmostEqual(a, b)
    # Check that the lists part works too
    a = [{"1": {"2": 1.000000000000001, "3": "a"}, "2": "1.23"}]
    b = [{"1": {"2": 1.000000000000002, "3": "a"}, "2": "1.23"}]
    assert not jsonAlmostEqual(a, b, rel_tol=1e-20)
    assert jsonAlmostEqual(a, b, rel_tol=1e-8)
    assert jsonAlmostEqual(a, b)
    # Check that the lists part works too
    a = [{"1": "a"}]
    b = []
    assert not jsonAlmostEqual(a, b)
    a = [{"1": "a"}, {"1": "a"}, {"2": "c"}]
    b = [{"1": "a"}, {"1": "a"}, {"2": "c"}]
    assert jsonAlmostEqual(a, b)


class TestCompany1:
    injection = 1
    target_rate = 1
    company = tank_level_company_factory('company1')
    critical_low_rate = -10
    marginal_low_rate = 0
    marginal_high_rate = 10
    critical_high_rate = 20
    critical_low_percent = 10
    marignal_low_percent = 20
    critical_low_DTE = 1
    marginal_low_DTE = 2
    meta_obj = Company1MetaData(-10, 0, 10, 20, 10, 20, 1, 2)
    tank_capacity = 10

    def test_get_pump_status_pump_failure(self):
        assert self.company.get_pump_status(0.9, 1, self.meta_obj) is PumpStatus.pump_failure

    def test_get_pump_status_pump_below_target(self):
        assert self.company.get_pump_status(0.95, 1, self.meta_obj) is PumpStatus.pump_below_target

    def test_get_pump_status_pump_good_on_good_low_boundary(self):
        assert self.company.get_pump_status(1, 1, self.meta_obj) is PumpStatus.good

    def test_get_pump_status_pump_good_in_range(self):
        assert self.company.get_pump_status(1.05, 1, self.meta_obj) is PumpStatus.good

    def test_get_pump_status_over_pumping_on_low_boundary(self):
        assert self.company.get_pump_status(1.1, 1, self.meta_obj) is PumpStatus.over_pumping

    def test_get_pump_status_runaway_pump(self):
        assert self.company.get_pump_status(1.2, 1, self.meta_obj) is PumpStatus.runaway_pump

    def test_get_pump_status_none_meta_obj_returns_good(self):
        meta_obj = Company1MetaData(critical_low_rate=None, marginal_low_rate=None, marginal_high_rate=None,
                                    critical_high_rate=None)
        assert self.company.get_pump_status('whatever', 1, meta_obj) is PumpStatus.good

    def test_get_tank_status_critical_volume(self):
        assert self.company.get_tank_status(1, 10, self.meta_obj, self.tank_capacity) is TankStatus.critical_low_volume

    def test_get_tank_status_marginal_volume(self):
        assert self.company.get_tank_status(2, 10, self.meta_obj, self.tank_capacity) is TankStatus.marginal_low_volume

    def test_get_tank_status_critical_DTE(self):
        assert self.company.get_tank_status(10, 1, self.meta_obj, self.tank_capacity) is TankStatus.critical_low_DTE

    def test_get_tank_status_marginal_DTE(self):
        assert self.company.get_tank_status(10, 2, self.meta_obj, self.tank_capacity) is TankStatus.marginal_low_DTE

    def test_get_tank_status_good_volume_is_good_dte_is_good(self):
        assert self.company.get_tank_status(10, 10, self.meta_obj, self.tank_capacity) is TankStatus.good

    def test_get_tank_status_none_meta_obj_returns_good(self):
        meta_obj = Company1MetaData(critical_low_percent=None, marignal_low_percent=None, critical_low_DTE=None,
                                    marginal_low_DTE=None)
        assert self.company.get_tank_status('whatever', 'whatever', meta_obj, self.tank_capacity) is TankStatus.good


class TestStatusEnum:
    def test_is_worse_than_function(self):
        assert not PumpStatus.runaway_pump.is_worse_than(TankStatus.critical_low_volume)
        assert not PumpStatus.runaway_pump.is_worse_than(TankStatus.critical_low_volume)
        assert not PumpStatus.over_pumping.is_worse_than(TankStatus.critical_low_volume)
        assert not PumpStatus.pump_below_target.is_worse_than(TankStatus.critical_low_volume)
        assert not PumpStatus.pump_failure.is_worse_than(TankStatus.critical_low_volume)
        assert not PumpStatus.good.is_worse_than(TankStatus.critical_low_volume)

        assert PumpStatus.runaway_pump.is_worse_than(TankStatus.marginal_low_volume)
        assert not PumpStatus.over_pumping.is_worse_than(TankStatus.marginal_low_volume)
        assert not PumpStatus.pump_below_target.is_worse_than(TankStatus.marginal_low_volume)
        assert PumpStatus.pump_failure.is_worse_than(TankStatus.marginal_low_volume)
        assert not PumpStatus.good.is_worse_than(TankStatus.marginal_low_volume)

        assert not PumpStatus.runaway_pump.is_worse_than(TankStatus.critical_low_DTE)
        assert not PumpStatus.over_pumping.is_worse_than(TankStatus.critical_low_DTE)
        assert not PumpStatus.pump_below_target.is_worse_than(TankStatus.critical_low_DTE)
        assert not PumpStatus.pump_failure.is_worse_than(TankStatus.critical_low_DTE)
        assert not PumpStatus.good.is_worse_than(TankStatus.critical_low_DTE)

        assert PumpStatus.runaway_pump.is_worse_than(TankStatus.marginal_low_DTE)
        assert not PumpStatus.over_pumping.is_worse_than(TankStatus.marginal_low_DTE)
        assert not PumpStatus.pump_below_target.is_worse_than(TankStatus.marginal_low_DTE)
        assert PumpStatus.pump_failure.is_worse_than(TankStatus.marginal_low_DTE)
        assert not PumpStatus.good.is_worse_than(TankStatus.marginal_low_DTE)

        assert PumpStatus.runaway_pump.is_worse_than(TankStatus.good)
        assert PumpStatus.over_pumping.is_worse_than(TankStatus.good)
        assert PumpStatus.pump_below_target.is_worse_than(TankStatus.good)
        assert PumpStatus.pump_failure.is_worse_than(TankStatus.good)
        assert not PumpStatus.good.is_worse_than(TankStatus.good)