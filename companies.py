import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
c_handler = logging.StreamHandler()
c_handler.setLevel(logging.INFO)
logger.addHandler(c_handler)

from models import (Company1MetaData,
                    DefaultMetaData, )
from more_itertools import always_iterable
from enums import TankStatus, PumpStatus
import numbers
from enum import Enum


class TankLevelCompanies(Enum):
    company1 = 'company1'
    company2 = 'company2'
    default_company = 'Default Company / Company Not Set'


def tank_level_company_factory(company_name):
    """
    Maybe make a static method inside DefaultTankLevelCompany?
    In the future will likely have an abstract company_factory, which tank_level_company_factory can inherit
    """
    if company_name == TankLevelCompanies.company1.value:
        return Company1()
    # elif company_name == TankLevelCompanies.company2.value:
    #     return Company2()
    return DefaultTankLevelCompany()


class DefaultTankLevelCompany:
    def __init__(self):
        # from reporting.views import MetaCreateView, MetaRetrieveUpdateView
        # from reporting.serializers import DefaultMetaCreateSerializer, DefaultMetaUpdateSerializer
        self.customer_name = TankLevelCompanies.default_company  # As is in the table accounts_custmors
        self.metadata = DefaultMetaData  # Name of metadata table
        # self.createview = MetaCreateView.as_view()
        # self.retrieveupdateview = MetaRetrieveUpdateView.as_view()
        # self.create_serializer = DefaultMetaCreateSerializer
        # self.update_serializer = DefaultMetaUpdateSerializer
        self.metadata_device_foreign_key = 'related_device'
        self.id = None

    def getSerializer(self, serializer, *args, **kwargs):
        # If serializer is not a function, just return that
        if not callable(serializer):
            return serializer
        # If the view has optional serializers
        return serializer(*args, **kwargs)

    def filterMetaDataByDevices(self, devices):
        return self.metadata.objects.filter(related_device__in=always_iterable(devices))

    def get_pump_status(self, injection, target_rate, meta_obj):
        raise NotImplementedError()

    def get_tank_status(self, vol, dte, meta_obj, tank_capacity):
        raise NotImplementedError()

    def get_site_status(self, tank, tank_capacity, target_rate, device, streamname, new_injection):
        raise NotImplementedError()


# TODO: fix so we can get from the user to eliminate a query
# def get_company_name_given_imei(user=None, imei=None):
def get_company_name_given_imei(imei):
    # if (user is not None) and (user.company.name != R3MOTE_COMPANY):
    #     return user.company
    # from digi.models import Device
    return Device.objects.values_list('customer_id', flat=True).filter(imei=imei)[:1].get()


class Company1(DefaultTankLevelCompany):
    def __init__(self):
        super().__init__()
        self.company_name = TankLevelCompanies.company1.value
        self.metadata = Company1MetaData
        self.id = 19

    def get_site_status(self, tank, tank_capacity, target_rate, device, streamname, new_injection):
        # If injection is 0, or some value that's not a number
        if (not isinstance(tank.avg_injection_rate2, numbers.Integral)) or (tank.avg_injection_rate2 == 0):
            dte = 10000
        else:
            dte = tank.volume / tank.avg_injection_rate2
        meta_obj = self.metadata.objects.get(streamname=streamname)
        pump_status = self.get_pump_status(tank.avg_injection_rate2, target_rate, meta_obj)
        tank_status = self.get_tank_status(tank.volume, dte, meta_obj, tank_capacity)
        # Assume that tank status has priority over pump status
        # So only if the pump status integer is worse (less) than the tank_status do we show that
        if pump_status.is_worse_than(tank_status):
            return pump_status.alert_text, pump_status.alert_level
        return tank_status.alert_text, tank_status.alert_level

    def get_tank_status(self, vol, dte, meta_obj, tank_capacity):
        logger.debug(f'volume: {vol}')
        logger.debug(f'dte: {dte}')
        logger.debug(f'tank capacity: {tank_capacity}')
        critical_percent = self.get_tank_percent(tank_capacity, meta_obj.critical_low_percent)
        logger.debug(f'critical_percent: {critical_percent}')
        marignal_low_percent = self.get_tank_percent(tank_capacity, meta_obj.marignal_low_percent)
        logger.debug(f'marginal_percent: {marignal_low_percent}')
        critical_low_DTE = self.if_var_is_a_number_return_var_else_none(meta_obj.critical_low_DTE)
        logger.debug(f'critical_low_DTE: {critical_low_DTE}')
        marginal_low_DTE = self.if_var_is_a_number_return_var_else_none(meta_obj.marginal_low_DTE)
        logger.debug(f'marginal_DTE: {marginal_low_DTE}')
        if (critical_percent is not None) and (vol <= critical_percent):
            return TankStatus.critical_low_volume
        elif (critical_low_DTE is not None) and (dte <= critical_low_DTE):
            return TankStatus.critical_low_DTE
        elif (critical_percent is not None) and (marignal_low_percent is not None) and (
                critical_percent < vol <= marignal_low_percent):
            return TankStatus.marginal_low_volume
        elif (critical_low_DTE is not None) and (marginal_low_DTE is not None) and (
                critical_low_DTE < dte <= marginal_low_DTE):
            return TankStatus.marginal_low_DTE
        return TankStatus.good

    def get_pump_status(self, injection, target_rate, meta_obj):
        """
        Get the pump status given the metadata and target injection rate
        """
        critical_low_rate = self.get_pump_rate(target_rate,
                                               meta_obj.critical_low_rate)  # Anything below target >10% would be critical (pump failure)
        marginal_low_rate = self.get_pump_rate(target_rate,
                                               meta_obj.marginal_low_rate)  # Anything below target <10% would be Marginal (marginal pump)
        marginal_high_rate = self.get_pump_rate(target_rate,
                                                meta_obj.marginal_high_rate)  # Anything on target or over by 10% or less good (good)
        critical_high_rate = self.get_pump_rate(target_rate,
                                                meta_obj.critical_high_rate)  # Anything 10-20% over Marginal (marginal pump)
        if (critical_low_rate is not None) and (injection <= critical_low_rate):
            return PumpStatus.pump_failure
        elif (critical_low_rate is not None) and (marginal_low_rate is not None) and (
                critical_low_rate < injection < marginal_low_rate):
            return PumpStatus.pump_below_target
        elif (marginal_low_rate is not None) and (critical_high_rate is not None) and (
                marginal_high_rate <= injection < critical_high_rate):
            return PumpStatus.over_pumping
        elif (critical_high_rate is not None) and (critical_high_rate <= injection):
            return PumpStatus.runaway_pump
        return PumpStatus.good

    # Can all just be nested functions, leave for now though
    @staticmethod
    def get_pump_rate(target_rate, meta_obj_rate):
        # If the percentage is not a number return none
        if not isinstance(meta_obj_rate, numbers.Integral):
            return None
        return target_rate * (1 + (meta_obj_rate / 100))

    @staticmethod
    def if_var_is_a_number_return_var_else_none(var):
        # IF the val is a number, return it, else return None
        return var if (isinstance(var, numbers.Integral)) else None

    @staticmethod
    def get_tank_percent(tank_capacity, percentage):
        # If the percentage is not a number return none
        if not isinstance(percentage, numbers.Integral):
            return None
        return tank_capacity * percentage / 100