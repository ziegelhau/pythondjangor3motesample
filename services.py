import datetime


def clearCache(url):
    # Clear cache by appending additional new query
    if url[-1] != '/':
        return url + '&1=' + str(datetime.datetime.now())
    return url + '?1=' + str(datetime.datetime.now())