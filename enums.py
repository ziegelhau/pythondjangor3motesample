from enum import Enum, IntEnum


class AlertLevel(IntEnum):
    critical = -1
    marginal = 0
    good = 1


class StatusEnum(Enum):
    @property
    def alert_level(self):
        return self.value[1]

    @property
    def alert_text(self):
        return self.value[0]

    def is_worse_than(self, other_status):
        return True if self.alert_level < other_status.alert_level else False


class TankStatus(StatusEnum):
    critical_low_volume = ('Critical Volume', AlertLevel.critical)
    marginal_low_volume = ('Marginal Volume', AlertLevel.marginal)
    critical_low_DTE = ('Critical DTE', AlertLevel.critical)
    marginal_low_DTE = ('Marginal DTE', AlertLevel.marginal)
    good = ('Good', AlertLevel.good)


class PumpStatus(StatusEnum):
    pump_failure = ('Pump Failure', AlertLevel.critical)
    runaway_pump = ('Runaway Pump', AlertLevel.critical)
    pump_below_target = ('Pump Below Target', AlertLevel.marginal)
    over_pumping = ('Pump Above Target', AlertLevel.marginal)
    good = ('Good', AlertLevel.good)