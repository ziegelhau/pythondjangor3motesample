from math import isclose
from numbers import Number


def jsonAlmostEqual(json1, json2, rel_tol=1e-8):
    """
    Custom function to compare that 2 list of dictionaries are almost equal (not a built in thing apparently)
    If dictionary value is a number, then check that the numbers are almost equal, otherwise check if values are exactly equal
    Note: does not currently try converting strings to digits and comparing them
    Does not care about ordering of keys in the dictionary
    Just returns true or false
    TODO: use assert in here with custom messages
        ALlow for other objects to be used (or just lists)
    """
    if type(json1) != list:
        json1 = [json1]
    if type(json2) != list:
        json2 = [json2]
    if len(json1) != len(json2):
        return False
    for i in range(len(json1)):
        dict1 = json1[i]
        dict2 = json2[i]
        if len(dict1) != len(dict2):
            return False
        # Loop through each item in the first dict and compare it to the second dict
        for key, item in dict1.items():
            # If it is a nested dictionary, need to call the function again
            if isinstance(item, dict):
                # If the nested dictionaries are not equal, return False
                if not jsonAlmostEqual(dict1[key], dict2[key], rel_tol=rel_tol):
                    return False
            # If it's not a dictionary, then continue comparing
            # Put in else statement or else the nested dictionary will get compared twice and
            # On the second time will check for exactly equal and will fail
            else:
                # If the value is a number, check if they are approximately equal
                if isinstance(item, Number):
                    # if not abs(dict1[key] - dict2[key]) <= rel_tol:
                    # https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
                    if not isclose(dict1[key], dict2[key], rel_tol=rel_tol):
                        return False
                else:
                    if not (dict1[key] == dict2[key]):
                        return False
    return True


# Iterate over list or dictionary
# https://stackoverflow.com/questions/12325608/iterate-over-a-dict-or-list-in-python
# def seq_iter(obj):
#     return obj if isinstance(obj, dict) else range(len(obj))

def listAlmostEqual(list1, list2, rel_tol=1e-8):
    """
    Check that 2 lists are almost equal
    Does not do nesting
    TODO combine with jsonAlmostEqual, make objAlmostEqual
    """
    if type(list1) != list:
        raise ValueError('list 1 is not a list')
    if type(list2) != list:
        raise ValueError('list 2 is not a list')
    if len(list1) != len(list2):
        return False
    for i in range(len(list1)):
        # If the value is a number, check if they are approximately equal
        if isinstance(list1[i], Number):
            # if not abs(dict1[key] - dict2[key]) <= rel_tol:
            # https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
            if not isclose(list1[i], list2[i], rel_tol=rel_tol):
                return False
        else:
            if not (list1[i] == list2[i]):
                return False
    return True