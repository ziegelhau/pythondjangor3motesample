class DefaultMetaData:
    pass


class Company1MetaData:
    """
    Metadata for Company1, added and overwrote any that differ from the base metadata
    """

    def __init__(self, critical_low_rate=None, marginal_low_rate=None, marginal_high_rate=None, critical_high_rate=None,
                 critical_low_percent=None, marignal_low_percent=None, critical_low_DTE=None, marginal_low_DTE=None):
        self.critical_low_rate = critical_low_rate
        self.marginal_low_rate = marginal_low_rate
        self.marginal_high_rate = marginal_high_rate
        self.critical_high_rate = critical_high_rate
        self.critical_low_percent = critical_low_percent
        self.marignal_low_percent = marignal_low_percent
        self.critical_low_DTE = critical_low_DTE
        self.marginal_low_DTE = marginal_low_DTE

    class Meta:
        verbose_name_plural = 'Company1 Metadata'